let images = document.querySelectorAll('.image-to-show'); let current = 1;
let continueBtn = document.querySelector('.continue'); continueBtn.disabled = true;
let stopBtn = document.querySelector('.stop');

function changeImg() {
    document.querySelector(".active").classList.remove('active');
    images[current].classList.add('active');
    if (current < images.length - 1) {
        current++;
    }
    else {
        current = 0;
    }
};

function btnListeners() {
    stopBtn.addEventListener('click', ()=>{
        clearInterval(a);
        continueBtn.disabled = false;
        stopBtn.disabled = true;
    });
    continueBtn.addEventListener('click', ()=>{
        a = setInterval(changeImg, 3000);
        stopBtn.disabled = false;
        continueBtn.disabled = true;
    });
}

btnListeners();
let a = setInterval(changeImg, 3000);