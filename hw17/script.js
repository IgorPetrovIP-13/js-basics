class Student {

    #name;
    #lastName;
    #tabel;
    #marks;

    constructor(name, lastName, tabel) {
        this.#name = name;
        this.#lastName = lastName;
        this.#tabel = tabel;
        this.#marks = Object.values(tabel);
    }

    CheckBadMarks() {
        if(this.#marks.every((value) => value >= 4)) {
            return 'Студента переведено на наступний курс';
        }
        return 'Студент має заборгованість';
    }

    GPA () {
        if ((this.#marks.reduce((a, b) => a + b, 0) / this.#marks.length)) {
            return 'Студенту призначено стипендію';
        }
        return 'Студент не має стипендії';
    }
}

let name = prompt('Student name') || 'NoName';
let lastName = prompt('Student last name') || 'NoLastName';
let tabel = {};
while(true) {
    let subject = prompt('Subject name') || 'Unidentified';
    let mark = +prompt('Mark');
    if (!mark) {
        break;
    }
    tabel[subject] = mark;
}

let newStudent = new Student(name, lastName, tabel);
console.log(newStudent);
console.log(newStudent.CheckBadMarks());
console.log(newStudent.GPA());