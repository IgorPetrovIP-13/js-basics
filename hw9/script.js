function appendLi(array, parent=document) {
    for (value of array) {
        if (Array.isArray(value)) {
            appendLi(value, parent);
        }
        else {
            parent.insertAdjacentHTML('beforeEnd', `<li>${value}</li>`); 
        }
    }
}

function createUl(array, parent=document.body) {
    let list = document.createElement('ul');
    list.setAttribute('id', 'created list')
    appendLi(array, list);
    parent.appendChild(list);
}

function timerRemove(seconds, elementToRemove) {
    let timer = document.createElement('h3');
    elementToRemove.after(timer);
    timer.innerText = `Time left: ${seconds} seconds`;
    const countdown = setInterval(()=>{
        seconds--;
        timer.innerText = `Time left: ${seconds} seconds`;
        if (seconds <= 0) {
            clearInterval(countdown);
            elementToRemove.remove();
            document.body.innerHTML = '';
        }
    }, 1000)
}

createUl(["Kharkiv", "Kiev", ["Borispol", "Irpin", ["London"]], "Odessa", "Lviv", "Dnieper"]);
timerRemove(3, document.getElementById('created list'));