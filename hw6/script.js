function createNewUser() {
    const newUser = {
        _name: '',
        _surname: '',
        _birthday: new Date(),
        setName: (name) => {this._name = name},
        setSurname: (surname) => {this._surname = surname},
        setBirthday: (birthday) => {
            const parts = birthday.split('.');
            this._birthday = new Date(parts[2], parts[1] - 1, parts[0])
        },
        getLogin: () => {return this._name.toLowerCase()[0] + this._surname.toLowerCase()},
        getAge: () => {
            let cur_date = new Date();
            if (cur_date.getMonth() > this._birthday.getMonth() || 
            (cur_date.getMonth() === this._birthday.getMonth() && cur_date.getDate() >= this._birthday.getDate())){
                return cur_date.getFullYear() - this._birthday.getFullYear();
            }
            return cur_date.getFullYear() - this._birthday.getFullYear() - 1;
        },
        getPassword: () => {return this._name.toUpperCase()[0] + this._surname.toLowerCase() + this._birthday.getFullYear()}
    };
    newUser.setName(prompt('Your name') || 'name');
    newUser.setSurname(prompt('Your surname') || 'surname');
    let birthday_date;
    do {
        birthday_date = prompt('Enter your birthday date');
    } while (!/^\d{2}\.\d{2}\.\d{4}$/.test(birthday_date));
    newUser.setBirthday(birthday_date);
    return newUser;
}

const user = createNewUser();
console.log('Login:', user.getLogin());
console.log('Password:', user.getPassword());
console.log('Age:', user.getAge());