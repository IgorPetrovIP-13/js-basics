function createWrapper(d) {
  let wrapper = document.createElement('div');
  wrapper.style.display = 'grid';
  wrapper.style.gridTemplateRows = 'repeat(10, 1fr)';
  wrapper.style.gridTemplateColumns = 'repeat(10, 1fr)';
  wrapper.style.width = 'fit-content';
  wrapper.style.margin = 'auto';
  wrapper.style.gap = '3px';
  return wrapper;
}

function createCircle(d) {
  let newCircle = document.createElement('div');
  newCircle.style.border = "1px solid black";
  newCircle.style.width = d + 'px';
  newCircle.style.height = d + 'px';
  newCircle.style.borderRadius = '50%'
  newCircle.style.backgroundColor = '#' + Math.floor(Math.random()*16777215).toString(16);
  newCircle.className = 'circle';
  return newCircle;
}

const createAll = (d) => {
  const wrapper = createWrapper(d);
  document.body.appendChild(wrapper);
  for (let i=0; i < 100; i++){
    wrapper.appendChild(createCircle(d));
  }
  wrapper.addEventListener('click', (ev)=> {
    if (ev.target.className === 'circle') {
      wrapper.removeChild(ev.target);
    }
  })
}

const addButtonListener = () => {
  let myBtn = document.querySelector('#drawCircles');
  myBtn.addEventListener('click', ()=>{
    let num;
    do {
      num = +prompt('Circle d:');
    } while (isNaN(num) || num <= 0 || !Number.isInteger(num));
    myBtn.remove();
    createAll(num);
  })
}

addButtonListener();