function validateNumber(number) {
    while(isNaN(number) || !number) {
        number = prompt('Reenter number')
    };
    return +number;
}

function validateOperation(operation) {
    while(!['+','-','*','/'].includes(operation.trim())) {
        operation = prompt('Reenter operation')
    };
    return operation;
}

function calcNumbers(a=0, b=0, operation='+') {
    switch(operation) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
    }
}

let num1 = validateNumber(prompt('Enter first number'));
let num2 = validateNumber(prompt('Enter second number'));
let operation = validateOperation(prompt('Enter operation'));
console.log(`(${num1}) ${operation} (${num2}) = ${calcNumbers(num1, num2, operation)}`);