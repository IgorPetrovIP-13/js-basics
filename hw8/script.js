for (let elem of document.getElementsByTagName('p')) {
    elem.style['background-color'] = "#ff0000";
}

let option_list = document.getElementById('optionsList');
console.log(option_list);
console.log(option_list.parentElement);
for (let elem of option_list.childNodes) {
    if (elem.tagName != undefined) {
        console.log(elem.nodeType, elem.tagName)
    }
}

document.getElementById('testParagraph').innerText = 'This is a paragraph';

for (let elem of document.querySelectorAll('.main-header>*')) {
    console.log(elem);
}

for (let elem of document.getElementsByClassName('section-title')) {
    elem.classList.remove('section-title');
}