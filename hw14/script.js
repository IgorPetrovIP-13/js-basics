const wrapper = document.querySelector('.wrapper');

const enable = () => {
    wrapper.classList.add("dark-mode");
    localStorage.setItem("dark-mode", "enabled");
};
  
const disable = () => {
    wrapper.classList.remove("dark-mode");
    localStorage.setItem("dark-mode", "disabled");
};

const checkDarkMode = () => {
    console.log(1)
    if (localStorage.getItem("dark-mode") === "enabled") {
        enable();
    }
    document.querySelector('#change-theme').addEventListener("click", (e) => {
        darkMode = localStorage.getItem("dark-mode");
        if (darkMode === "disabled") {
            enable();
        } else {
            disable();
        }
    });
}

checkDarkMode();