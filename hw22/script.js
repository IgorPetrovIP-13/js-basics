const cellWidth = 20;

function createWrapper() {
  let wrapper = document.createElement('div');
  wrapper.style.display = 'grid';
  wrapper.style.gridTemplateRows = 'repeat(30, 1fr)';
  wrapper.style.gridTemplateColumns = 'repeat(30, 1fr)';
  wrapper.style.width = 'fit-content';
  wrapper.style.margin = '150px auto 0';
  wrapper.style.border = '1px solid black';
  return wrapper;
}

function createCell() {
  let cell = document.createElement('div');
  cell.style.border = "1px solid black";
  cell.style.width = cellWidth + 'px';
  cell.style.height = cellWidth + 'px';
  return cell;
}

const createAll = () => {
  const wrapper = createWrapper();
  document.body.appendChild(wrapper);
  for (let i=0; i < 900; i++){
    wrapper.appendChild(createCell());
  }
  document.body.addEventListener('mousedown', (ev)=>{
    wrapper.childNodes.forEach((el) => el.classList.toggle('black'));
  });
  wrapper.addEventListener('mousedown', (ev)=> {
    ev.target.classList.toggle('black');
    ev.stopPropagation();
  });
}
createAll();