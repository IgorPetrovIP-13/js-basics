const test = {
    name: "John",
    age: 30,
    address: {
      street: "123 Main St",
      city: "Anytown",
      state: "CA",
      zip: "12345"
    },
    hobbies: ["reading", "running", "gardening"],
    pets: [
      {
        name: "Fido",
        species: {
            "dogs": "",
            "cats": []
        },
        age: 5
      },
      {
        name: "Whiskers",
        species: "cat",
        age: 3
      }
    ]
  };

function deepClone(oldObj) {
    let newObj = {};
    for (let propName in oldObj) {
        if (Array.isArray(oldObj[propName])) {
            newObj[propName] = oldObj[propName].map(element => {
                if (typeof element === 'object' && element !== null) {
                    return deepClone(element);
                }
                else {
                    return element; 
                }
            });
        }
        else if (typeof oldObj[propName] === 'object' && oldObj[propName] !== null) {
            newObj[propName] = deepClone(oldObj[propName]);
        }
        else {
            newObj[propName] = oldObj[propName];
        }
    }
    return newObj;
}

console.log(test);
console.log(deepClone(test));