function validateNumber() {
    let number;
    do{
        number = prompt('Enter number');
    } while(isNaN(number) || !number || number < 0);
    return +number;
}

function factorial(num) {
    return num === 1 ? num : num*factorial(num-1);
}

let num = validateNumber();
if (num === 0) {
    alert(1);
}
else {
    alert(factorial(num));
}