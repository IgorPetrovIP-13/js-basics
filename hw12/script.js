function add_listener() {
    let pressed;
    let highlighted;
    document.addEventListener('keydown', (ev)=> {
        pressed = document.querySelector(`#${ev.code.toLocaleLowerCase()}`);
        if (pressed) {
            highlighted = document.querySelector('.blue');
            if(highlighted) {
                highlighted.classList.remove('blue')
            }
            pressed.classList.add('blue');
        }
    })
}

add_listener();