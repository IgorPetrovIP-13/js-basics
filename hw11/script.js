function show_password_btn() {
    let input;
    document.querySelectorAll('.icon-password').forEach((el) => {
        el.addEventListener('click', () => {
                input = document.querySelector(`[data-btn = ${el.id}]`);
                if (input.type === 'password') {
                    input.type = 'text';
                    el.classList.replace('fa-eye', 'fa-eye-slash');
                }
                else {
                    input.type = 'password';
                    el.classList.replace('fa-eye-slash', 'fa-eye');
                }
        })
    })
}

function create_text() {
    let text = document.createElement('p');
    text.style.cssText = `
        position: absolute;
        color: red;
        font-size: 0.8rem;
        pointer-events: none;
        padding: 0;
        margin-top: -20px;
        opacity: 0%;
    `;
    text.textContent = 'Потрібно ввести однакові значення';
    return text;
}

function confirm_btn() {
    let inputs = document.querySelectorAll('.submit.password-input');
    let text = create_text();
    inputs[inputs.length-1].insertAdjacentElement('afterend', text);
    document.querySelector('.btn').addEventListener('click', (ev)=> {
        ev.preventDefault()
        flag = true;
        for(i of inputs) {
            if ((i.value !== inputs[0].value) || i.value === '') {
                flag = false;
                break;
            }
        }
        if(flag) {
            text.style.opacity = '0%';
            setTimeout(()=>{alert('Welcome')}, 10);
        }
        else {
            text.style.opacity = '100%';
        }
    });
}

show_password_btn();
confirm_btn();