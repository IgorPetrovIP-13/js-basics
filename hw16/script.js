function validateNumber(text) {
    let number;
    do{
        number = prompt(text);
    } while(isNaN(number) || !number);
    return +number;
}

function fibonacci(f0=0, f1=1, n) {
    if (n === 0) return f0;
    if (n === 1) return f1;
    return fibonacci(f0, f1, n-1) + fibonacci(f0, f1, n-2);
}

let f0 = validateNumber('Enter F0');
let f1 = validateNumber('Enter  F1');
while(f1 < f0) {
    f1 = validateNumber('Enter  F1');
}
let n = validateNumber('Enter  n');
console.log(fibonacci(f0, f1, n));