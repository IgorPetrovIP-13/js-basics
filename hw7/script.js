function filterBy(array, type) {
    if (type === 'null'){
        return array.filter((value) => value !== null);
    };
    return array.filter((value) => (type !== typeof value) || (value === null));
}

let array = [1, 'n', null, undefined, NaN, {a: 4}, true];
console.log(filterBy(array, 'number'));
console.log(filterBy(array, 'object'));
console.log(filterBy(array, 'null'));