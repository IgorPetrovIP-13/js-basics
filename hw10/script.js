function create_event_listener() {
    document.querySelector('.tabs').addEventListener('click', ev => {
        document.querySelector('.tabs-title.active').classList.remove('active');
        ev.target.classList.add('active');
        document.querySelector('.tab.active').classList.remove('active');
        document.querySelector(`[data-li = ${ev.target.id}]`).classList.add('active');
    });
}

create_event_listener();